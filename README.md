# @chammy/plugin-manager

[![pipeline status](https://gitlab.com/chammyjs/plugin-manager/badges/master/pipeline.svg)](https://gitlab.com/chammyjs/plugin-manager/commits/master)
[![coverage report](https://gitlab.com/chammyjs/plugin-manager/badges/master/coverage.svg)](https://gitlab.com/chammyjs/plugin-manager/commits/master)
[![npm (scoped)](https://img.shields.io/npm/v/@chammy/plugin-manager.svg)](https://npmjs.com/package/@chammy/plugin-manager)

Simple plugin manager for [Chammy.js](https://gitlab.com/chammyjs/chammy).

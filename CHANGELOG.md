# @chammy/plugin-manager Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

---

## [0.2.0]
### Added
* Add changelog.
* [#6]: Support for Node 12.

### Removed
* **BREAKING:** [#6]: Support for Node 6.

### Fixed
* Wrong version of library in banner comment of distributed package.

## 0.1.0 – 2017-12-10
### Added
* First working version, yay!


[#6]: https://gitlab.com/chammyjs/plugin-manager/issues/6

[0.2.0]: https://gitlab.com/chammyjs/plugin-manager/compare/v0.1.0...v0.2.0
